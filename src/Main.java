import controller.Controlador;
import view.Insercion;
import view.Login;
import view.Menu;
import view.Registro;

/**
 * Created by miquelabellan on 20/4/17.
 */
public class Main {

    public static void main(String[] args) {
        Login login = new Login();
        Insercion insercion = new Insercion();
        Registro registro = new Registro();
        login.setVisible(true);

        Menu menu = new Menu();
        Controlador controlador = new Controlador(login, menu, registro, insercion);
        login.registraControlador(controlador, controlador, controlador);
        menu.registraControlador(controlador, controlador);
        insercion.registrarControlador(controlador, controlador);
        registro.registrarControlador(controlador, controlador);
    }
}
