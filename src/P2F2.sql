-- use p2;

delimiter $$
drop procedure if exists registrar_usuario $$
create procedure registrar_usuario(in usuario varchar(255), in contrasenya varchar(255), out exito boolean)
begin
	declare existe boolean;
    set existe = (select count(mysql.user.user) from mysql.user where mysql.user.user like usuario) = 1;
    if existe then
		set exito = false;
    else
		set @aux = concat("create user '", usuario, "' identified by '", contrasenya, "'");
        prepare stmt from @aux;
        execute stmt;
        deallocate prepare stmt;
        
		set @aux = concat("grant select, insert, update, delete on p2.* to '", usuario, "' with grant option");
        prepare stmt from @aux;
        execute stmt;
        deallocate prepare stmt;
        
        set @aux = concat("grant create user on *.* to '", usuario, "' with grant option");
        prepare stmt from @aux;
        execute stmt;
        deallocate prepare stmt;
        
        set @aux = concat("grant select on mysql.user to '", usuario, "'");
        prepare stmt from @aux;
        execute stmt;
        deallocate prepare stmt;
        
        set @aux = concat("grant execute on p2.* to '", usuario, "'");
        prepare stmt from @aux;
        execute stmt;
        deallocate prepare stmt;
        
        flush privileges;
		set exito = true;
    end if;
end $$

delimiter ;

-- call registrar_usuario('d', '+', @v);

-- select @v;

DELIMITER $$
DROP PROCEDURE IF EXISTS p2.insereix_info $$
CREATE PROCEDURE p2.insereix_info(IN medallion      VARCHAR(511), IN hack_license VARCHAR(511),
                                IN vendor_id VARCHAR(511), IN rate_code INT, IN store_and_fwd_flag CHAR, IN pickup_datetime TIMESTAMP, 
                                IN dropoff_datetime TIMESTAMP, IN passenger_count INT, IN trip_time_in_secs LONG, IN trip_distance DOUBLE, 
                                IN pickup_longitude DOUBLE, IN pickup_latitude DOUBLE, IN dropoff_longitude DOUBLE, IN dropoff_latitude DOUBLE, 
                                IN payment_type VARCHAR(255), IN fare_amount DOUBLE, IN surcharge DOUBLE, IN mta_tax DOUBLE, 
                                IN tip_amount DOUBLE, IN tolls_amount DOUBLE, IN total_amount DOUBLE, OUT exito boolean)
BEGIN
        DECLARE existe BOOLEAN;
  SET existe = (SELECT COUNT(t.vendor_id)
                FROM Taxi AS t
                WHERE t.vendor_id = vendor_id AND t.hack_license = hack_license AND t.vendor_id = vendor_id) >= 1;

        IF !existe THEN

          CALL p2.verificar_tipus_pagament(payment_type);

          INSERT INTO p2.Taxi (medallion, hack_license, vendor_id)
            VALUES( medallion, hack_license, vendor_id);

          INSERT INTO p2.viatge (tarifa, Store_and_fwd_flag, inici, fi, passatgers, temps, distancia, lat_inici, lon_inici, lat_fi, lon_fi, medallion, hack_license, vendor_id)
            VALUES (rate_code,Store_and_fwd_flag, inici, fi, passenger_count, trip_time_in_secs, trip_distance, pickup_latitude, pickup_longitude, dropoff_latitude, dropoff_longitude, medallion, hack_license, vendor_id);

          INSERT INTO p2.preu (medallion, hack_license, vendor_id, data_recollida,
                               tipus_pagament, preu, recarrec, taxa_mta, propina, peatge, preu_total)
          VALUES (medallion, hack_license, vendor_id, pickup_datetime,
                             payment_type, total_amount, surcharge, mta_tax, tip_amount, tolls_amount, total_amount);
            SET exito = true;
        ELSE 
            SET exito = false;
        END IF;
END $$
DELIMITER ;

-- CALL insereix_info(1,311,8, 4,'c','2000-01-01 00:00:00','2000-01-01 00:00:00',0, 3, 1000, 100.12, 90.1, 91.4, 90.2, 89.0, 0, 10.2, 0.5, 0, 0, 0);


-- select * from mysql.user;


CALL insereix_info("asdasdasd", "asdasd", "asdddddd", 1, 'c', '2000-01-01 00:00:00', '2000-01-01 00:00:00', 1, 2, 3.1,
                   1.1, 1.1, 1.2, 1.2, "dfg", 1.1, 1.2, 1.1, 1.1, 1.1, 2.0, @exito);
SELECT @exito;

delimiter $$
DROP TRIGGER IF EXISTS p2.insertar_viatge $$
CREATE TRIGGER p2.insertar_viatge
AFTER INSERT ON p2.viatge
for each row begin
  INSERT INTO p2olap.viatje
  VALUES (new.medallion, new.hack_license, new.vendor_id, new.tarifa, new.store_and_fwd_flag, new.inici, new.fi,
                         new.passatgers, new.temps, new.distancia, new.lat_inici, new.lon_fi, new.lat_fi, new.lon_fi);
end;
$$
delimiter ;

DELIMITER $$
DROP TRIGGER IF EXISTS p2.insertar_preu $$
CREATE TRIGGER p2.insertar_preu
AFTER INSERT ON p2.preu
FOR EACH ROW
  BEGIN

    DECLARE id_tipo INT;
    SET id_tipo = (SELECT p.id_tipus_pagament
                   FROM p2olap.pagament AS p
                   WHERE p.tipus_pagament = new.tipus_pagament);

    INSERT INTO p2olap.preu
    VALUES (new.medallion, new.hack_license, new.vendor_id, new.data_recollida, id_tipo, new.preu, new.recarrec,
                           new.taxa_mta, new.propina,
                           new.peatge, new.preu_total);
  END;
$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS p2.verificar_tipus_pagament $$
CREATE PROCEDURE p2.verificar_tipus_pagament(IN payment_type VARCHAR(255))
  BEGIN
    DECLARE trobat BOOLEAN;
    DECLARE tipus_pag_aux INT;

    SET trobat = (SELECT COUNT(p.id_tipus_pagament)
                  FROM p2olap.pagament AS p
                  WHERE p.tipus_pagament = payment_type) >= 1;

    IF !trobat
    THEN
      INSERT p2olap.pagament (tipus_pagament) VALUES (payment_type);
    END IF;
  END $$
DELIMITER ;

/*
delimiter $$
drop procedure if exists insertar_olap $$
create procedure insertar_olap(IN medallion VARCHAR(511), IN hack_license VARCHAR(511),
                                IN vendor_id VARCHAR(511), IN rate_code INT, IN store_and_fwd_flag CHAR, IN pickup_datetime TIMESTAMP, 
                                IN dropoff_datetime TIMESTAMP, IN passenger_count INT, trip_time_in_secs LONG, IN trip_distance DOUBLE, 
                                IN pickup_longitude DOUBLE, IN pickup_latitude DOUBLE, IN dropoff_longitude DOUBLE, IN dropoff_latitude DOUBLE, 
                                IN payment_type VARCHAR(255), IN fare_amount DOUBLE, IN surcharge DOUBLE, IN mta_tax DOUBLE, 
                                IN tip_amount DOUBLE, IN tolls_amount DOUBLE, IN total_amount DOUBLE)
begin
    DECLARE trobat BOOLEAN;
    DECLARE tipus_pag_aux INT;

    SET trobat = (SELECT COUNT(pagament.id_tipus_pagament) FROM pagament AS p WHERE p.tipus_pagament = payment_type )>1;

    IF !trobat THEN
        INSERT p2olap.pagament(tipus_pagament) VALUES (payment_type);
    END IF;

    SET tipus_pag_aux = (SELECT id_tipus_pagament FROM pagament AS p WHERE p.tipus_pagament = payment_type);

	insert into p2olap.viatje values(medallion, hack_license, vendor_id, rate_code, store_and_fwd_flag, pickup_datetime, dropoff_datetime, 
    passenger_count, trip_time_in_secs, trip_distance, pickup_latitude, pickup_longitude, dropoff_latitude, dropoff_longitude);

	insert into p2olap.preu 
	values(medallion, hack_license, vendor_id, pickup_datetime, tipus_pag_aux, fare_amount, surcharge, mta_tax, tip_amount, 
    tolls_amount, total_amount);


end $$
delimiter ;*/