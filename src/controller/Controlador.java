package controller;

import model.BBDD;
import view.Insercion;
import view.Login;
import view.Menu;
import view.Registro;

import javax.swing.*;
import java.awt.event.*;

/**
 * Created by miquelabellan on 20/4/17.
 */
public class Controlador extends WindowAdapter implements ActionListener, KeyListener {

    private BBDD bbdd;
    private Login login;
    private Menu menu;
    private Registro registro;
    private Insercion insercion;

    public Controlador(Login login, Menu menu, Registro registro, Insercion insercion) {
        this.login = login;
        this.menu = menu;
        this.registro = registro;
        this.insercion = insercion;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "LOGIN":
                bbdd = BBDD.conectar(login.getUsername(), login.getPassword());
                if (bbdd == null) {
                    msgBox(login, "No se ha podido iniciar sesión. Verifica los datos.", JOptionPane.WARNING_MESSAGE);
                } else {
                    hideShow(login, menu);
                }
                break;

            case "REGISTRAR":
                if (bbdd.crearUsuario(registro.getUser(), registro.getPassword())) {
                    msgBox(registro, "Usuario creado correctametente.", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    msgBox(registro, "Error al crear el usuario, ya existe.", JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "FINESTRA REGISTRE":
                hideShow(menu, registro);
                break;

            case "FINESTRA INSERIR":
                hideShow(menu, insercion);
                break;

            case "INSERTAR":
                if (bbdd.anadirInformacion(insercion.getMedallion(), insercion.getHack_license(), insercion.getVendor_id(),
                        insercion.getRate_code(), insercion.getStore_and_fwd_flag(), insercion.getPickup_datetime(),
                        insercion.getDropoff_datetime(), insercion.getPassanger_count(), insercion.getTrip_time_in_secs(),
                        insercion.getTrip_distance(), insercion.getPickup_longitude(), insercion.getPickup_latitude(),
                        insercion.getDropoff_longitude(), insercion.getDropoff_latitude(), insercion.getPayment_type(),
                        insercion.getFare_amount(), insercion.getSurcharge(), insercion.getMta_tax(), insercion.getTip_amount(),
                        insercion.getTolls_amount(), insercion.getTotal_amount())) {
                    msgBox(insercion, "Insertado correctamente", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    msgBox(insercion, "No se pudo inserir la información", JOptionPane.WARNING_MESSAGE);
                }
                break;
        }
    }

    private void hideShow(JFrame hide, JFrame show) {
        hide.setVisible(false);
        show.setVisible(true);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        Object src = e.getSource();
        if (src instanceof Login) {
            if (bbdd != null && bbdd.estaConectado()) {
                bbdd.desconectar();
            }
            System.exit(0);
        } else {
            if (src instanceof Menu) {
                bbdd.desconectar();
                login.setVisible(true);
            } else {
                menu.setVisible(true);
            }
        }
    }

    private void msgBox(JFrame parent, String body, int type) {
        JOptionPane.showMessageDialog(parent, body, "", type);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            login.getLoginButton().doClick();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }
}
