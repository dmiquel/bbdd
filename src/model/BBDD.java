package model;

import java.sql.*;

/**
 * Created by Daniel on 20/4/17.
 */
public class BBDD {

    private final static String URL_CONECTAR_BBDD = "jdbc:mysql://localhost:3306/p2?useSSL=false&noAccessToProcedureBodies=true";

    private final String QUERY_CREAR_USUARIO = "call registrar_usuario(?, ?, ?);";
    private final int PARAM_IN_USUARIO = 1;
    private final int PARAM_IN_PASSWORD = 2;
    private final int PARAM_OUT_EXITO = 3;

    private final String QUERY_INSEREIX_INFO = "call insereix_info(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
            " ?, ?, ?, ?, ?, ?, ?);";
    private final int MEDALLION = 1;
    private final int HACK_LICENSE = 2;
    private final int VENDOR_ID = 3;
    private final int RATE_CODE = 4;
    private final int STORE_AND_FWD_FLAG = 5;
    private final int PICKUP_DATETIME= 6;
    private final int DROPOFF_DATETIME = 7;
    private final int PASS_COUNT = 8;
    private final int TRIP_TIME = 9;
    private final int TRIP_DISTANCE = 10;
    private final int PICKUP_LONG = 11;
    private final int PICKUP_LAT = 12;
    private final int DROPOFF_LONG = 13;
    private final int DROPOFF_LAT = 14;
    private final int PAYMENT_TYPE = 15;
    private final int FARE_AMOUNT = 16;
    private final int SURCHARGE = 17;
    private final int MTA_TAX = 18;
    private final int TIP_AMOUNT = 19;
    private final int TOLLS_AMOUNT = 20;
    private final int TOTAL_AMOUNT = 21;
    private final int PARAM_OUT_EXIT = 22;


    private Connection conn;

    private BBDD(String user, String password) throws SQLException {
        conn = DriverManager.getConnection(URL_CONECTAR_BBDD, user, password);
    }

    public static BBDD conectar(String user, String password) {
        try {
            return new BBDD(user, password);
        } catch (SQLException ex) {
            return null;
        }
    }

    public boolean crearUsuario(String user, String password) {
        try {
            CallableStatement crear = conn.prepareCall(QUERY_CREAR_USUARIO);
            crear.setString(PARAM_IN_USUARIO, user);
            crear.setString(PARAM_IN_PASSWORD, password);
            crear.registerOutParameter(PARAM_OUT_EXITO, Types.BOOLEAN);
            crear.execute();
            return crear.getBoolean(PARAM_OUT_EXITO);
        } catch (SQLException e) {
            return false;
        }
    }

    public void desconectar() {
        try {
            conn.close();
        } catch (SQLException e) {
            //Nada
        }
    }

    public boolean estaConectado() {
        try {
            return !conn.isClosed();
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean anadirInformacion(String medallion, String hack_license, String vendor_id, int rate_code,
                                     char store_and_forward, String pickup_datetime, String dropoff_datetime,
                                     int passenger_count, long trip_time, double trip_distance, double pickup_long,
                                     double pickup_lat, double dropoff_long, double dropoff_lat, String payment_type,
                                     double fare_amount, double surcharge, double mta_tax, double tip_amount,
                                     double tolls_amount, double total_amount) {
        try {
            CallableStatement crear = conn.prepareCall(QUERY_INSEREIX_INFO);
            crear.setString(MEDALLION, medallion);
            crear.setString(HACK_LICENSE, hack_license);
            crear.setString(VENDOR_ID, vendor_id);
            crear.setInt(RATE_CODE, rate_code);
            crear.setString(STORE_AND_FWD_FLAG, Character.toString(store_and_forward));
            crear.setString(PICKUP_DATETIME, pickup_datetime);
            crear.setString(DROPOFF_DATETIME, dropoff_datetime);
            crear.setInt(PASS_COUNT, passenger_count);
            crear.setLong(TRIP_TIME, trip_time);
            crear.setDouble(TRIP_DISTANCE, trip_distance);
            crear.setDouble(PICKUP_LONG, pickup_long);
            crear.setDouble(PICKUP_LAT, pickup_lat);
            crear.setDouble(DROPOFF_LONG, dropoff_long);
            crear.setDouble(DROPOFF_LAT, dropoff_lat);
            crear.setString(PAYMENT_TYPE, payment_type);
            crear.setDouble(FARE_AMOUNT, fare_amount);
            crear.setDouble(SURCHARGE, surcharge);
            crear.setDouble(MTA_TAX, mta_tax);
            crear.setDouble(TIP_AMOUNT, tip_amount);
            crear.setDouble(TOLLS_AMOUNT, tolls_amount);
            crear.setDouble(TOTAL_AMOUNT, total_amount);
            crear.registerOutParameter(PARAM_OUT_EXIT, Types.BOOLEAN);
            crear.execute();
            return crear.getBoolean(PARAM_OUT_EXIT);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}