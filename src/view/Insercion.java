package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

/**
 * Created by Daniel on 20/4/17.
 */
public class Insercion extends JFrame {

    private JPanel contentPane;
    private JPanel panel;
    private JPanel panel_1;
    private JLabel label;
    private JTextField medallion;
    private JLabel label_1;
    private JTextField hack_license;
    private JLabel label_2;
    private JTextField vendor_id;
    private JLabel label_3;
    private JTextField rate_code;
    private JLabel label_4;
    private JTextField store_and_fwd_flag;
    private JLabel label_5;
    private JTextField pickup_datetime;
    private JLabel label_6;
    private JTextField dropoff_datetime;
    private JLabel label_7;
    private JTextField passanger_count;
    private JLabel lblTriptimeinsecs;
    private JTextField trip_time_in_secs;
    private JLabel lblTripdistance;
    private JTextField trip_distance;
    private JLabel lblPickuplongitude;
    private JTextField pickup_longitude;
    private JLabel lblPickuplatitude;
    private JTextField pickup_latitude;
    private JLabel lblDropofflongitude;
    private JTextField dropoff_longitude;
    private JLabel lblDropofflatitude;
    private JTextField dropoff_latitude;
    private JLabel lblPaymenttype;
    private JTextField payment_type;
    private JPanel panel_2;
    private JLabel lblFareamount;
    private JTextField fare_amount;
    private JLabel lblSurcharge;
    private JTextField surcharge;
    private JLabel lblMtatax;
    private JTextField mta_tax;
    private JLabel lblTipamount;
    private JTextField tip_amount;
    private JLabel lblTollsamount;
    private JTextField tolls_amount;
    private JLabel lblTotalamount;
    private JTextField total_amount;
    private JButton btnInsertar;

    public Insercion() {
        setTitle("Insertar");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 617, 443);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(1, 3, 0, 0));

        panel = new JPanel();
        contentPane.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        label = new JLabel("medallion");
        panel.add(label);

        medallion = new JTextField();
        medallion.setColumns(10);
        panel.add(medallion);

        label_1 = new JLabel("hack_license");
        panel.add(label_1);

        hack_license = new JTextField();
        hack_license.setColumns(10);
        panel.add(hack_license);

        label_2 = new JLabel("vendor_id");
        panel.add(label_2);

        vendor_id = new JTextField();
        vendor_id.setColumns(10);
        panel.add(vendor_id);

        label_3 = new JLabel("rate_code");
        panel.add(label_3);

        rate_code = new JTextField();
        rate_code.setColumns(10);
        panel.add(rate_code);

        label_4 = new JLabel("store_and_fwd_flag");
        panel.add(label_4);

        store_and_fwd_flag = new JTextField();
        store_and_fwd_flag.setColumns(10);
        panel.add(store_and_fwd_flag);

        label_5 = new JLabel("pickup_datetime");
        panel.add(label_5);

        pickup_datetime = new JTextField();
        pickup_datetime.setColumns(10);
        panel.add(pickup_datetime);

        label_6 = new JLabel("dropoff_datetime");
        panel.add(label_6);

        dropoff_datetime = new JTextField();
        dropoff_datetime.setColumns(10);
        panel.add(dropoff_datetime);

        label_7 = new JLabel("passenger_count");
        panel.add(label_7);

        passanger_count = new JTextField();
        passanger_count.setColumns(10);
        panel.add(passanger_count);

        panel_1 = new JPanel();
        contentPane.add(panel_1);
        panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

        lblTriptimeinsecs = new JLabel("trip_time_in_secs");
        panel_1.add(lblTriptimeinsecs);

        trip_time_in_secs = new JTextField();
        trip_time_in_secs.setColumns(10);
        panel_1.add(trip_time_in_secs);

        lblTripdistance = new JLabel("trip_distance");
        panel_1.add(lblTripdistance);

        trip_distance = new JTextField();
        trip_distance.setColumns(10);
        panel_1.add(trip_distance);

        lblPickuplongitude = new JLabel("pickup_longitude");
        panel_1.add(lblPickuplongitude);

        pickup_longitude = new JTextField();
        pickup_longitude.setColumns(10);
        panel_1.add(pickup_longitude);

        lblPickuplatitude = new JLabel("pickup_latitude");
        panel_1.add(lblPickuplatitude);

        pickup_latitude = new JTextField();
        pickup_latitude.setColumns(10);
        panel_1.add(pickup_latitude);

        lblDropofflongitude = new JLabel("dropoff_longitude");
        panel_1.add(lblDropofflongitude);

        dropoff_longitude = new JTextField();
        dropoff_longitude.setColumns(10);
        panel_1.add(dropoff_longitude);

        lblDropofflatitude = new JLabel("dropoff_latitude");
        panel_1.add(lblDropofflatitude);

        dropoff_latitude = new JTextField();
        dropoff_latitude.setColumns(10);
        panel_1.add(dropoff_latitude);

        lblPaymenttype = new JLabel("payment_type");
        panel_1.add(lblPaymenttype);

        payment_type = new JTextField();
        payment_type.setColumns(10);
        panel_1.add(payment_type);

        lblFareamount = new JLabel("fare_amount");
        panel_1.add(lblFareamount);

        fare_amount = new JTextField();
        panel_1.add(fare_amount);
        fare_amount.setColumns(10);

        panel_2 = new JPanel();
        contentPane.add(panel_2);
        panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

        lblSurcharge = new JLabel("surcharge");
        panel_2.add(lblSurcharge);

        surcharge = new JTextField();
        surcharge.setColumns(10);
        panel_2.add(surcharge);

        lblMtatax = new JLabel("mta_tax");
        panel_2.add(lblMtatax);

        mta_tax = new JTextField();
        mta_tax.setColumns(10);
        panel_2.add(mta_tax);

        lblTipamount = new JLabel("tip_amount");
        panel_2.add(lblTipamount);

        tip_amount = new JTextField();
        tip_amount.setColumns(10);
        panel_2.add(tip_amount);

        lblTollsamount = new JLabel("tolls_amount");
        panel_2.add(lblTollsamount);

        tolls_amount = new JTextField();
        tolls_amount.setColumns(10);
        panel_2.add(tolls_amount);

        lblTotalamount = new JLabel("total_amount");
        panel_2.add(lblTotalamount);

        total_amount = new JTextField();
        total_amount.setColumns(10);
        panel_2.add(total_amount);

        btnInsertar = new JButton("Insertar");
        panel_2.add(btnInsertar);
    }


    public void registrarControlador(ActionListener al, WindowListener wl) {
        addWindowListener(wl);
        btnInsertar.addActionListener(al);
        btnInsertar.setActionCommand("INSERTAR");
    }

    public String getMedallion() {
        return medallion.getText();
    }

    public String getHack_license() {
        return hack_license.getText();
    }

    public String getVendor_id() {
        return vendor_id.getText();
    }

    public int getRate_code() {
        return Integer.valueOf(rate_code.getText());
    }

    public char getStore_and_fwd_flag() {
        return store_and_fwd_flag.getText().charAt(0);
    }

    public String getPickup_datetime() {
        return pickup_datetime.getText();
    }

    public String getDropoff_datetime() {
        return dropoff_datetime.getText();
    }

    public int getPassanger_count() {
        return Integer.valueOf(passanger_count.getText());
    }

    public long getTrip_time_in_secs() {
        return Long.valueOf(trip_time_in_secs.getText());
    }

    public double getTrip_distance() {
        return Double.valueOf(trip_distance.getText());
    }

    public double getPickup_longitude() {
        return Double.valueOf(pickup_longitude.getText());
    }

    public double getPickup_latitude() {
        return Double.valueOf(pickup_latitude.getText());
    }

    public double getDropoff_longitude() {
        return Double.valueOf(dropoff_longitude.getText());
    }

    public double getDropoff_latitude() {
        return Double.valueOf(dropoff_latitude.getText());
    }

    public String getPayment_type() {
        return payment_type.getText();
    }

    public double getFare_amount() {
        return Double.valueOf(fare_amount.getText());
    }

    public double getSurcharge() {
        return Double.valueOf(surcharge.getText());
    }

    public double getMta_tax() {
        return Double.valueOf(mta_tax.getText());
    }

    public double getTip_amount() {
        return Double.valueOf(tip_amount.getText());
    }

    public double getTolls_amount() {
        return Double.valueOf(tolls_amount.getText());
    }

    public double getTotal_amount() {
        return Double.valueOf(total_amount.getText());
    }
}
