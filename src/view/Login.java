package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;

/**
 * Created by miquelabellan on 20/4/17.
 */
public class Login extends JFrame {

    private JButton jbLogin;
    private JTextField jtfUsername;
    private JPasswordField jtfPassword;

    public Login() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        JPanel jpTotal = new JPanel();
        jpTotal.setLayout(new BoxLayout(jpTotal, BoxLayout.PAGE_AXIS));

        JPanel jpUsername = new JPanel();
        jpUsername.setLayout(new FlowLayout());
        JLabel jlUsername = new JLabel();
        jlUsername.setText("Username: ");
        jtfUsername = new JTextField();
        jtfUsername.setColumns(50);
        jpUsername.add(jlUsername);
        jpUsername.add(jtfUsername);

        JPanel jpPassword = new JPanel();
        jpPassword.setLayout(new FlowLayout());
        JLabel jlPassword = new JLabel();
        jlPassword.setText("Password: ");
        jtfPassword = new JPasswordField();
        jtfPassword.setColumns(50);
        jpPassword.add(jlPassword);
        jpPassword.add(jtfPassword);

        jbLogin = new JButton();
        jbLogin.setText("Login");

        jpTotal.add(jpUsername);
        jpTotal.add(jpPassword);
        jpTotal.add(jbLogin);

        setSize(650, 150);
        this.add(jpTotal);
        setLocationRelativeTo(null);
    }

    public void registraControlador(ActionListener al, WindowListener wl, KeyListener kl) {
        addWindowListener(wl);
        jbLogin.addActionListener(al);
        jbLogin.setActionCommand("LOGIN");
        jtfPassword.addKeyListener(kl);
    }

    public String getUsername() {
        return jtfUsername.getText();
    }

    public String getPassword() {
        return new String(jtfPassword.getPassword());
    }

    public JButton getLoginButton() {
        return jbLogin;
    }
}
