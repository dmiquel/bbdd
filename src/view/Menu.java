package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

/**
 * Created by miquelabellan on 20/4/17.
 */
public class Menu extends JFrame {

    private JButton jbCrearUsuari;
    private JButton jbInserirDades;

    public Menu() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        JPanel jpTotal = new JPanel();
        jpTotal.setLayout(new FlowLayout());

        jbCrearUsuari = new JButton();
        jbCrearUsuari.setText("Crear Usuario");

        jbInserirDades = new JButton();
        jbInserirDades.setText("Insertar datos");

        jpTotal.add(jbCrearUsuari);
        jpTotal.add(jbInserirDades);
        setSize(150, 120);
        this.add(jpTotal);
        setLocationRelativeTo(null);
    }

    public void registraControlador(ActionListener actionListener, WindowListener wl) {
        addWindowListener(wl);
        jbCrearUsuari.addActionListener(actionListener);
        jbCrearUsuari.setActionCommand("FINESTRA REGISTRE");

        jbInserirDades.addActionListener(actionListener);
        jbInserirDades.setActionCommand("FINESTRA INSERIR");
    }
}

