package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

/**
 * Created by Daniel on 20/4/17.
 */
public class Registro extends JFrame {

    private JPanel contentPane;
    private JTextField txtUser;
    private JPasswordField txtPassword;
    private JButton button;

    public Registro() {
        setTitle("Registrar");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 194);
        setLocationRelativeTo(null);
        contentPane = new JPanel();

        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        ((BorderLayout) contentPane.getLayout()).setVgap(5);
        JLabel lblRegistro = new JLabel("Registro");
        lblRegistro.setFont(new Font("Lucida Grande", Font.PLAIN, 17));
        lblRegistro.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblRegistro, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel lblUser = new JLabel("Usuario");
        panel.add(lblUser);

        txtUser = new JTextField();
        panel.add(txtUser);
        txtUser.setColumns(10);

        JLabel lblContrasea = new JLabel("Contraseña");
        panel.add(lblContrasea);

        txtPassword = new JPasswordField();
        panel.add(txtPassword);
        txtPassword.setColumns(10);

        JPanel pnl = new JPanel();
        panel.add(pnl);

        button = new JButton("Registrar");
        pnl.add(button);

    }

    public void registrarControlador(ActionListener al, WindowListener wl) {
        addWindowListener(wl);
        button.addActionListener(al);
        button.setActionCommand("REGISTRAR");
    }

    public String getUser() {
        return txtUser.getText();
    }


    public String getPassword() {
        return new String(txtPassword.getPassword());
    }
}
